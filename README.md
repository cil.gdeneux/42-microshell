
Standard function to get a string length

```c
int		ft_strlen(char *str)
{
	int i = 0;
	while (str[i])
		i++;
	return (i);
}
```

Used for print errors and quit when fatal error occured.
Close read_output_fd if opened (In case of value is not -1).

```c
void	ft_error(char *str1, char *str2, int fatal)
{
	if (str1 != NULL)
		write(STDERR_FILENO, str1, ft_strlen(str1));
	if (str2 != NULL)
		write(STDERR_FILENO, str2, ft_strlen(str2));
	write(STDERR_FILENO, "\n", 2);
	if (read_output_fd != -1)
	{
		close(read_output_fd);
		read_output_fd = -1;
	}
	if (fatal)
		exit(0);
}
```

Built-in function "cd"

```c
void	ft_cd(char *path, int argument_count)
{
	if (argument_count != 2)
		ft_error("error: cd: bad arguments", NULL, 0);
	if (chdir(path) != 0)
		ft_error("error: cd: cannot change directory to ", path, 0);
}
```

Execute "args" as command using execve.
First check if "read_output_fd" is opened (In case of value != -1), then
use "read_output_fd" as input command.
Next, if "end" the output will be redirected into the pipe.

Set "read_output_fd"'s value to the new pipe (in side) file descriptor to read
from it at the next command.

Don't forget to close the file desciptors child-side and parent-side, only the "parent-side" new pipe "in" side
will be closed after used it as the next command output.

```c
void	ft_execute_cmd(char **args, char **envp, int end)
{
	int fd[2];

	if (pipe(fd) != 0)
		ft_error("error: fatal", NULL, 1);
	int pid = fork();
	if (pid < 0)
	{
		close(fd[0]);
		close(fd[1]);
		ft_error("error: fatal", NULL, 1);
	}
	if (pid == 0)
	{
		close(fd[0]);
		if (read_output_fd != -1)
		{
			dup2(read_output_fd, STDIN_FILENO);
			close(read_output_fd);
			read_output_fd = -1;
		}
		if (!end)
			dup2(fd[1], STDOUT_FILENO);
		close(fd[1]);
		if (execve(args[0], args, envp) == -1)
			ft_error("error: cannot execute ", args[0], 1);
	}
	read_output_fd = fd[0];
	close(fd[1]);
	wait(NULL);
}
```




















